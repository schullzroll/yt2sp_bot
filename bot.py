import json
import logging
import re
import sys
from os import environ as env
from typing import List
from typing import Union
from typing import Set
from math import sqrt, pow, exp

import discord
import spotipy
from discord.ext import commands
from spotipy.oauth2 import SpotifyClientCredentials
from youtube_title_parse import get_artist_title
from youtube_api import YouTubeDataAPI

from src.db import get_conn, run_sql

log_handler = logging.StreamHandler(sys.stdout)
log = logging.getLogger('discord')
log.setLevel(logging.DEBUG)
log.addHandler(log_handler)

def map_over(obj, functions: List[callable], options=None):
    if options is None:
        # assigning default argument in the function signature only creates one instance
        options = dict()
    temp = obj
    for f in functions:
        temp = f(temp, **options)
    return temp


# expects arg to curry to be the last of positional
def curry(function, *args, **kwargs):
    return lambda arg: function(*args, arg, **kwargs)


def extract_video_url(message):
    # Match the video ID in the full YouTube URL or a shortened URL
    pattern = r'(youtube\.com\/watch\?v=|youtu\.be\/|youtube\.com\/embed\/)([A-Za-z0-9_-]+)'
    match = re.search(pattern, message)

    if match:
        video_url = ''.join(match.groups())
        log.info(f"Found youtube video url {video_url} in \"{message}\"")
        return video_url, match.groups()[1]
    else:
        return None


yt = YouTubeDataAPI(env["YOUTUBE_DEVELOPER_KEY"])

def get_yt_artist_and_song(url, id):
    metadata = yt.get_video_metadata(id)
    if not metadata:
        raise Exception("Not a Youtube video id")
    title = metadata.get("video_title")
    channel_name = metadata.get("channel_title")

    channel_name = map_over(channel_name, [
        curry(re.sub, r"\s*-\s*Topic", ''),
        curry(re.sub, r"\([^)]*\)", ''),
        curry(re.sub, r"\[[^\]]*\]", '')
    ]).strip()

    # author = response['items'][0]['snippet']['author']
    # duration = response['items'][0]['contentDetails']['duration']
    # thumbnail_url = response['items'][0]['snippet']['thumbnails']['default']['url']
    title = map_over(title, [
        curry(re.sub, r"\([^)]*\)", ''),
        curry(re.sub, r"\[[^\]]*\]", ''),
        curry(re.sub, r"\|[^\|]*\|", '')
    ]).strip()

    if parsed_artist_title := get_artist_title(title):
        method = "title artist"
        use_artist, use_title = parsed_artist_title
    else:
        method = "channel artist"
        use_artist, use_title = channel_name, title

    log.info(f"Video \"{url}\" title: \"{use_title}\" artist: \"{use_artist}\" using method: \"{method}\"")
    return use_artist, use_title


spotify_cid = env["SPOTIFY_CID"]
spotify_secret = env["SPOTIFY_SECRET"]

# Authentication - without user
client_credentials_manager = SpotifyClientCredentials(client_id=spotify_cid, client_secret=spotify_secret)
sp = spotipy.Spotify(client_credentials_manager=client_credentials_manager)

from typing import Set

def levenshtein_distance(s1: str, s2: str) -> int:
    if len(s1) < len(s2):
        s1, s2 = s2, s1
    
    previous_row = list(range(len(s2) + 1))
    for i, c1 in enumerate(s1):
        current_row = [i + 1]
        for j, c2 in enumerate(s2):
            insertions = previous_row[j + 1] + 1
            deletions = current_row[j] + 1
            substitutions = previous_row[j] + (c1 != c2)
            current_row.append(min(insertions, deletions, substitutions))
        previous_row = current_row
    
    return previous_row[-1]

def similarity(artistsA: Set[str], trackA: str, artistsB: Set[str], trackB: str) -> float:
    # Normalize artist names (lowercase and sorted)
    norm_artistsA = " ".join(sorted(a.lower().strip() for a in artistsA))
    norm_artistsB = " ".join(sorted(a.lower().strip() for a in artistsB))
    
    # share artists, highly likely it is the same song
    if artistsA & artistsB:
        artist_score = 1.0
    else:
        # Compute similarity for artists
        artist_distance = levenshtein_distance(norm_artistsA, norm_artistsB)
        artist_score = 1 - artist_distance / max(len(norm_artistsA), len(norm_artistsB), 1)
    
    # Compute similarity for track names
    track_distance = levenshtein_distance(trackA.lower(), trackB.lower())
    track_score = 1 - track_distance / max(len(trackA), len(trackB), 1)
    
    # Weighted average similarity (equal weight to artists and track name)
    return (artist_score + track_score) / 2

similarity_threshold = float(env.get("SIMILARITY", "0.2"))

def lookup_spotify_track(artist, name, manual_query=None):
    query = manual_query or f"artist:{artist} track:{name}"
    log.info(f"Running spotify query \"{query}\"")
    # search_dict = sp.search(urllib.parse.quote(query), limit=1) or {}
    search_dict = sp.search(query, limit=1, type="track") or {}
    found_tracks = search_dict["tracks"]["items"]
    if (not search_dict) or (not found_tracks):
        log.info(f"No track found for query \"{query}\"")
        return None
    track = found_tracks[0]
    artists = {artist["name"] for artist in track["artists"]}
    track_id = track['id']
    track_link = track["external_urls"]["spotify"] if len(found_tracks) > 0 else ""
    spotify_track_name = map_over(track["name"], [
        curry(re.sub, r"\s*-.+remaster", '', flags=re.IGNORECASE)
    ]).strip()
    #                    youtube         spotify
    split_artists = set()
    for by in [",", "&", "ft.", "Ft.", "Feat.", "feat."]:
        split_artists |= set(artist.split(by))
    similar = similarity(split_artists, name, artists, spotify_track_name)
    similarity_explanation = f"{split_artists}:{name} vs {artists}:{spotify_track_name} returned similarity {similar}"
    log.info(similarity_explanation)
    if similar < similarity_threshold:
        log.info(f"Result for query \"{query}\" is link \"{track_link}\". Not similar enough {similar} < {similarity_threshold}")
        return None

    return track_link, search_dict, track_id, similar


spotify_cache = dict()


def spotify_track(artist: str, name: str):
    for options in [(artist, name), (artist, name, f"{artist} {name}")]:
        found = lookup_spotify_track(*options)
        if found:
            track_link = found[0]
            api_dict = found[1]
            track_id = found[2]
            similarity = found[3]
            return track_link, api_dict, track_id, similarity
    return None


def extract_spotify_track_url(message):
    # Match the track ID in the full Spotify URL
    pattern = r'(https\:\/\/open\.spotify\.com\/track\/)([A-Za-z0-9_-]+)'

    if match := re.search(pattern, message):
        video_url = ''.join(match.groups())
        log.info(f"Found spotify track url {video_url} in \"{message}\"")
        return match.groups()[1]
    return None


def get_sp_artist_and_song(track_id):
    # Extract artist and track names from spotify API
    api_dict = sp.track(track_id)
    use_artist = api_dict['artists'][0]['name']
    use_title = api_dict['name']
    url = api_dict['external_urls']['spotify']
    log.info(f"Track \"{url}\" title: \"{use_title}\" artist: \"{use_artist}\"")
    return use_artist, use_title, api_dict


def youtube_video(artist: str, track: str):
    # Search YouTube for video with artist and track names in title
    search_term = artist + " - " + track
    result = yt.search(q=search_term, max_results=1)
    id = result[0]["video_id"]
    return f"https://youtu.be/{id}", id

listen_to_this = int(env["MUSIC_SHARE_CHANNEL"])
bot_controls = int(env["BOT_CONTROLS_CHANNEL"])

intents = discord.Intents.default()
intents.message_content = True
bot = commands.Bot(command_prefix='/', intents=intents)


class Database:
    def __init__(self, conn=None):
        self.conn = conn or get_conn()

    def lookup_youtube_to_spotify_link(self, v_hash: str) -> Union[str, None]:
        query = "SELECT st.track_id \
                 FROM spotify_tracks st \
                 JOIN track_share ts ON st.track_id = ts.spotify \
                 JOIN youtube_tracks yt ON ts.youtube = yt.v_hash \
                 WHERE yt.v_hash = %s \
        "
        params = (v_hash,)
        ret = run_sql(self.conn.cursor(), query, params)
        link = None
        if len(ret) > 0:
            link = "https://open.spotify.com/track/" + ret[0][0]
        return link

    def lookup_spotify_to_youtube_link(self, track_id: str) -> Union[str, None]:
        query = "SELECT yt.v_hash \
                 FROM spotify_tracks st \
                 JOIN track_share ts ON st.track_id = ts.spotify \
                 JOIN youtube_tracks yt ON ts.youtube = yt.v_hash \
                 WHERE st.track_id = %s \
        "
        params = (track_id,)
        ret = run_sql(self.conn.cursor(), query, params)
        link = None
        if len(ret) > 0:
            link = "https://youtu.be/" + ret[0][0]
        return link

    def is_present_yt(self, video_id: str):
        params = (video_id,)
        query = "SELECT EXISTS(SELECT 1 FROM youtube_tracks WHERE v_hash = %s);"
        return run_sql(self.conn.cursor(), query, params)[0][0]

    def is_present_sp(self, track_id: str):
        params = (track_id,)
        query = "SELECT EXISTS(SELECT 1 FROM spotify_tracks WHERE track_id = %s);"
        return run_sql(self.conn.cursor(), query, params)[0][0]

    def save_sharing(self, author: str, vhash: str = None, sp_tid: str = None, sp_data: dict = None):
        success = bool(sp_tid) and bool(sp_data)
        already_there_yt = self.is_present_yt(vhash)
        already_there_sp = self.is_present_sp(sp_tid)
        queries = [
            (not already_there_yt, "INSERT INTO youtube_tracks (v_hash) VALUES (%s); COMMIT", (vhash,)),
            (
                not already_there_sp and success,
                "INSERT INTO spotify_tracks (track_id, api_data) VALUES (%s, %s); COMMIT",
                (sp_tid, json.dumps(sp_data))),
        ]
        for pred, query, params in queries:
            if pred:
                run_sql(self.conn.cursor(), query, params)

        query = "INSERT INTO track_share (author, youtube, spotify) VALUES (%s, %s, %s)"
        params = (author, vhash, sp_tid)
        run_sql(self.conn.cursor(), query, params)


database = Database()
monitor = {listen_to_this, bot_controls}


@bot.event
async def on_message(message: discord.Message):
    options = {"reference": message}
    if message.author.bot or message.channel.id not in monitor:
        return
    spotify_link = ""
    log.info(f"Processing message: {message.content}")
    # YouTube -> Spotify
    extracted = extract_video_url(message.content)
    if extracted:
        video_url = extracted[0]
        video_id = extracted[1]
        if spotify_link := database.lookup_youtube_to_spotify_link(video_id):
            log.info(f"Resolution from database {video_url} -> {spotify_link}")
            await message.channel.send(spotify_link, **options)
            await message.add_reaction("♻️")
            return

        artist, song = get_yt_artist_and_song(video_url, video_id)
        if data := spotify_track(artist, song):
            spotify_link = data[0]
            data_dict = data[1]
            track_id = data[2]
            similarity = data[3]
            database.save_sharing(message.author.id, video_id, track_id, data_dict)
            await message.channel.send(f"tuším na **{similarity * 100:.0f} %**, že je to\n{spotify_link}", **options)
        return
    # Spotify -> YouTube
    if spotify_song_id := extract_spotify_track_url(message.content):
        if video_url := database.lookup_spotify_to_youtube_link(spotify_song_id):
            log.info(f"Resolution from database {spotify_link} -> {video_url}")
            await message.channel.send(video_url, **options)
            await message.add_reaction("♻️")
            return
        song_artist, song_title, api_dict = get_sp_artist_and_song(spotify_song_id)
        if yt_data := youtube_video(song_artist, song_title):
            youtube_link, video_id = yt_data
            database.save_sharing(message.author.id, video_id, spotify_song_id, api_dict)
            await message.channel.send(youtube_link, **options)


token = env["DISCORD_BOT_TOKEN"]
log.info("Starting bot")
bot.run(token, log_handler=log_handler)
